# generator-tew Credits

## Core Thanks!

- [elioWay](https://elioway.gitlab.io)
- [Yeoman](http://yeoman.io/)

## Artwork

- [art](https://commons.wikimedia.org/)
- <https://publicdomainvectors.org/en/free-clipart/Robots-head/47649.html>
- <https://publicdomainvectors.org/en/free-clipart/Polaroid-print-vector-image/69892.html>
- <https://publicdomainvectors.org/en/free-clipart/Red-eyes/64486.html>
- <https://publicdomainvectors.org/en/free-clipart/Package-application/38773.html>
- <https://commons.wikimedia.org/wiki/File:Robot-cong-nghiep-the-he-moi.jpg>
- <https://free-images.com/display/wooden_robot_grass_lawn.html>
