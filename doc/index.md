<aside>
  <dl>
  <dd>Stand ready at command, and are his eyes</dd>
  <dd>That run through all the Heavens, or down to the Earth</dd>
  <dd>Bear his swift errands over moist and dry</dd>
</dl>
</aside>

A set of generators to work with elioWay repos, artwork, documentation, etc. These will replace **generator-art** and call **chisel**.

You'd use it like this:

```bash
yo tew:chisel elioway/eliosin
yo tew:art elioway/elioflesh/ember-flesh --icon ~/Downloads/publicdomain_icon.png --favicon ~/Downloads/publicdomain_favicon.png
```
