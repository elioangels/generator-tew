# Quickstart generator-tew

- [generator-tew Prerequisites](/elioangels/generator-tew/prerequisites.html)
- [Installing generator-tew](/elioangels/generator-tew/installing.html)

## Nutshell

When you are working with the **elioWay** repository, use this for generating artwork and documentation.

```bash
npm install -g yo
npm install -g generator-tew
yo tew:chisel elioway/eliosin
yo tew:art elioway/elioflesh/ember-flesh --icon ~/Downloads/publicdomain_icon.png --favicon ~/Downloads/publicdomain_favicon.png
```
