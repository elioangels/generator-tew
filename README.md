![](https://elioway.gitlab.io/elioangels/generator-tew/elio-generator-tew-logo.png)

> Take command, **the elioWay**

# generator-tew ![notstarted](https://elioway.gitlab.io/eliosin/icon/devops/notstarted/favicon.ico "notstarted")

A set of generators to work with elioWay repos, artwork, documentation, etc. These will replace **generator-art** and call **chisel**.

- [generator-tew Documentation](https://elioway.gitlab.io/elioangels/generator-tew/)

## Prerequisites

- [generator-tew Prerequisites](https://elioway.gitlab.io/elioangels/generator-tew/installing.html)

## Installing

- [Installing generator-tew](https://elioway.gitlab.io/elioangels/generator-tew/installing.html)

## Seeing is Believing

```bash
npm install -g yo
npm install -g generator-tew
yo tew:chisel elioway/eliosin
yo tew:art elioway/elioflesh/ember-flesh --icon ~/Downloads/publicdomain_icon.png --favicon ~/Downloads/publicdomain_favicon.png
```

- [elioangels Quickstart](https://elioway.gitlab.io/elioangels/quickstart.html)
- [generator-tew Quickstart](https://elioway.gitlab.io/elioangels/generator-tew/quickstart.html)

# Credits

- [generator-tew Credits](https://elioway.gitlab.io/elioangels/generator-tew/credits.html)

## License

[MIT](license)

![](https://elioway.gitlab.io/elioangels/generator-tew/apple-touch-icon.png)
